import React from 'react';
import {Link} from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import ActionHome from 'material-ui/svg-icons/action/home';


const Header = () => (
   
   <div id="btn-home" >
        <RaisedButton label='Home Page' containerElement={<Link to='/'/>}/>
   </div>
);

export default Header;