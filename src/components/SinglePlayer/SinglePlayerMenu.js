import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Settings from '../Settings/Settings';
import RaisedButton from 'material-ui/RaisedButton';

class SinglePlayerMenu extends Component{
    constructor(props){
        super(props);

        this.state=
        {
            difficulty:1, 
            time:1,
        };
    }

    setDifficulty(event,index,value){
        this.setState({
            difficulty: value
        });
    }

    setTime(event,index,value){
        this.setState({
            time: value
        });
    }

    render(){
        return(
            <div>
                <h1>Single Player Settings</h1>

                <Settings time={this.state.time} 
                          difficulty={this.state.difficulty} 
                          setDifficulty={this.setDifficulty.bind(this)} 
                          setTime={this.setTime.bind(this)}/>
                <br/>
                <RaisedButton label='Looks Good!' primary={true} containerElement={
                        <Link to={{
                            pathname: '/single/play',
                            state: {difficulty: this.state.difficulty, 
                                time: this.state.time}}}>Looks Good!
                        </Link>} />
            </div>
        );
    }
}

export default SinglePlayerMenu;