import React, {Component} from 'react';
import MathProblemsApi from '../../utilities/MathProblemsApi';
import Play from '../Play/Play';
import CountdownToGameStart from '../Play/CountdownToGameStart';

class Single extends Component{
    constructor(props){
        super(props);

        this.state = 
        {
            mathProblems: {}, 
            playerType:'single',
            hasGameStarted: false
        };
    }

    componentDidMount(){  
        //Create a new instance of the API that will be used to retrieve math problems      
        this.mathProblemsApi = new MathProblemsApi();

        //Retrieve the math problems that correspond to the received time and difficulty and set them in the state
        this.mathProblemsApi.getMathProblems(this.props.location.state.difficulty,this.props.location.state.time,(mathProblems) => {
            this.setState({mathProblems: mathProblems});
        })
    }

    setGameStarted(){
        this.setState({
            hasGameStarted: true
        });
    }

    render(){
        return (
            <div>
                {!this.state.hasGameStarted ?
                    <CountdownToGameStart waitUntilClick = {true} secondsRequested = {3} setGameStarted={this.setGameStarted.bind(this)}/>
                    :
                    <Play mathProblems = {this.state.mathProblems} isMultiplayerGame={false} time = {this.props.location.state.time}/>
                }
            </div>
        );
    }  
}

export default Single;
