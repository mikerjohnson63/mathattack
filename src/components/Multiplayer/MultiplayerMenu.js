import React, {Component} from 'react';
import Settings from '../Settings/Settings.js';
import {Link} from "react-router-dom";
import Multiplayer from './Multiplayer';
import MathProblemsApi from '../../utilities/MathProblemsApi.js';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const textFieldStyle = { width: 100};

class MultiplayerMenu extends Component{
    constructor(props){
        super(props);

        this.state = {
            gameId: '',
            joinGame: false,
            difficulty: 1,
            time: 1,
            userId: -1,
            createNewGame: false
    }
}

    setDifficulty(event,index,value){
        this.setState({
            difficulty: value
        });
    }

    setTime(event,index,value){
        this.setState({
            time: value
        });
    }

    updateGameValue(event){
        this.setState({gameId: event.target.value})
    }

    //Fires when the user requests to create a new game
    handleCreateGame(){
        const mathApi = new MathProblemsApi();
        mathApi.createNewGame(this.state.difficulty,this.state.time, (response) => {
            this.setState({
                gameId: response,
                createNewGame: true
            });
        });
    }

    render(){
        return(
            <div>
                <h1>Multiplayer Settings</h1>
                {this.state.createNewGame ?
                    <div>
                        <Settings setDifficulty={this.setDifficulty.bind(this)} 
                            setTime={this.setTime.bind(this)}
                            time={this.state.time}
                            difficulty={this.state.difficulty}/>
                        <br/>
                        <RaisedButton label='Looks Good!' primary={true} containerElement={
                            <Link to={{
                                pathname: '/multi/play',
                                state: {gameId: this.state.gameId,
                                        difficulty: this.state.difficulty, 
                                        time: this.state.time,
                                        isFromSettingsComponent: true}}}>Looks Good!
                            </Link>} />
                    </div>
                    : 
                    <div>
                        <span>
                            <RaisedButton onClick={this.handleCreateGame.bind(this)} label='Create Game' primary={true} />
                        </span>
                        <span className="padding-left-25">
                            <TextField hintText="Game id" 
                                       style={textFieldStyle} 
                                       id="txt-join-game" 
                                       onChange={ $event => this.updateGameValue($event)} 
                                       value={this.state.gameId} type="text"/>

                            <RaisedButton label='Join Game' primary={true} containerElement={
                                <Link to={{
                                    pathname: '/multi/play',
                                    state: {gameId: this.state.gameId,
                                        difficulty: this.state.difficulty,
                                        time: this.state.time,
                                        isFromSettingsComponent: false}}}>
                                </Link>} />
                        </span>
                    </div>
                     }
            </div>
        );
    }
}

export default MultiplayerMenu;
