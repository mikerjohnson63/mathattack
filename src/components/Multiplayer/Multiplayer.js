import React, {Component} from 'react';
import Play from '../Play/Play';
import MathProblemsApi from '../../utilities/MathProblemsApi';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import CountdownToGameStart from '../Play/CountdownToGameStart';
import RaisedButton from 'material-ui/RaisedButton';

//list of possible player names
const names = ['PorkChop','SugarBooger','Lemon','Twinkie',
    'Pooky','Panther','CowPie','Nutella','Pinecone','Popcorn','BubbleMaster',
    'Bloop','DragonBreath','BelchMaster','DreamCrusher','TreeHugger','Bandicoot',
    'Bigfoot','Sasquatch','Banshee','SandwichSlayer','MisterCrankyPants','TheDarkLord','Runaway',
    'Abolitionist','CheeseSniffer','NosePicker','LintCollector','Fluffy','CakeDestroyer',
    'PigWrestler','Cannibal','GarlicBreath','BootLicker','BigBoy','Savage','WaffleDestroyer','RedFlame',
    'SodaPop','CrabCake','Hunter','BunnyRabbit','CatSnuggler','CreamPuff','GateCrasher','SmallFry',
    'KingOfTheHill','RedCabbage','CouchPotato','GirlScout','VeggieBurger','CornHusker','CheekPincher',
    'PizzaKiller','PorkBelly','TipsyUnicorn','BlueNapalm','ToeJam','PeanutButter','FlabbyPanda','BreadAndButter',
    'BabyHippo','FishBait','SnakeBait','CrabCake','Majestic'];

class Multiplayer extends Component{
    constructor(props){
        super(props);

        this.state = {
            mathProblems: {},
            playersList: [],
            userId: -1,
            userName: '',
            correctAnswerCount: 0,
            incorrectAnswerCount: 0,
            isReadyToPlay: false,
            gameId: this.props.location.state.gameId,
            hasGameStarted: false,
            countDownHasStarted:false
        }
    }

    componentDidMount(){ 
        //const  socket = new SockJS('https://attack-server.cfapps.io/gs-guides-websocket');
        const  socket = new SockJS('http://localhost:8080/gs-guides-websocket');

        //Create the web socket connection
        this.stompClient = Stomp.over(socket);
        this.stompClient.connect({}, (frame) => {

            //create a subscription to add a new player
            this.stompClient.subscribe('/mathAttack/multiplayer/' + this.state.gameId + '/playerAdded', player => {
                let data = JSON.parse(player.body);

                if(this.state.userId === -1){
                    this.setState({
                        userId:data.id,
                        userName:data.userName,
                        incorrectAnswerCount: data.incorrectAnswerCount,
                        correctAnswerCount: data.correctAnswerCount,
                        isReadyToPlay: data.isReadyToPlay
                    })
                }

                //once the player is added, send a request to get an updated list of players
                this.stompClient.send('/app/gameServer/multiplayer/' + this.state.gameId + '/getPlayersInGame', {}, null);
            });
            
            //Create a username for the player and set it in the state
            this.setState({
                userName:this.generateUserName()
            });

            //send a request to add a new player
            this.stompClient.send('/app/gameServer/multiplayer/' + this.state.gameId + '/addPlayer', {}, userName);

            //Create a subscription to receive math problems
            this.stompClient.subscribe('/mathAttack/multiplayer/' + this.state.gameId + '/mathProblems', mathProblems =>{

                this.setState({
                    mathProblems: JSON.parse(mathProblems.body)
                })
            });

            //Create a subscription to start a new game
            this.stompClient.subscribe('/mathAttack/multiplayer/' + this.state.gameId + '/nextGame', updatedPlayerStats => {
                let parsedPlayerStats = JSON.parse(updatedPlayerStats.body);
                this.startNextGame(parsedPlayerStats);
            });

            //Create a subscription to get a list of players in the game
            this.stompClient.subscribe('/mathAttack/multiplayer/' + this.state.gameId + '/players', players => {
                let playersList = JSON.parse(players.body);
                this.setState({
                    playersList: playersList
                });

                //Start a count to see how many players are ready to play
                let playersReadyCount = 0;

                for(let i = 0; i < playersList.length; i++){
                    if(playersList[i].isReadyToPlay){
                        playersReadyCount++;
                    }
                }

                //if more than one player is ready to play, start the countdown for the game
                if(playersReadyCount > 1){
                    this.setState({
                        countDownHasStarted: true
                    });

                    if(this.props.location.state.isFromSettingsComponent){
                        this.stompClient.send('/app/gameServer/multiplayer/' + this.state.gameId + '/getMathProblems', 
                            {}, JSON.stringify({
                            'difficulty':this.props.location.state.difficulty, 'time':this.props.location.state.time
                        }));
                    }           
                }
            });      
        });
    }

    //Set the state for a new game
    startNextGame(playersList){
        this.setState({
            mathProblems: {},
            isReadyToPlay: false,
            hasGameStarted: false,
            countDownHasStarted:false,
            playersList: playersList
        });
    }

    //sets the state as started
    setGameStarted(){
        this.setState({
            hasGameStarted: true
        });
    }

    //Creates a name for a user
    generateUserName(){
        let phrase = names[Math.floor(Math.random()*names.length)];
        let number = Math.floor(Math.random()*100);
        return phrase + number;
    }

    //Called when a user is ready to play
    handleReadyToPlay(){
        this.mathProblemsApi = new MathProblemsApi();
        this.mathProblemsApi.setUserIsReadyToPlay(this.state.userId, (response) =>{

            this.stompClient.send('/app/gameServer/multiplayer/' + this.state.gameId + '/getPlayersInGame', {}, null);
            this.setState({
                isReadyToPlay: true
            });
        });   
    }

    render(){
        return(
            <div>
                {!this.state.hasGameStarted ?
                    <div>
                        <h1>Multiplayer Connections</h1>
                        <strong>Your user name is: {this.state.userName}</strong>
                        <br/>
                        <br/>
                        
                        {!this.state.countDownHasStarted ?
                            <div>
                                <p>Your friends can join the game using the code "<strong>{this.props.location.state.gameId}</strong>"</p>
                                <p>The game will begin when more than one person has clicked "Ready To Play!".</p>
                                <br/>
                                <div>{this.state.playersList.map((player,index) => (
                                    <div key={index}>
                                        <p>{player.userName} is {player.isReadyToPlay ? "ready to play" : "connected"}</p>
                                    </div>))} 
                                </div>
                                {!this.state.isReadyToPlay &&
                                    <RaisedButton primary={true} 
                                                  onClick={() => this.handleReadyToPlay()} 
                                                  label='Ready To Play!' />
                                }
                            </div>
                            :
                            <CountdownToGameStart gameId={this.state.gameId} 
                                                  waitUntilClick = {false} 
                                                  secondsRequested = {5} 
                                                  isMultiplayerGame={true} 
                                                  setGameStarted={this.setGameStarted.bind(this)}/>  
                        }
                    </div>  
                    :
                    <div>
                        <Play userName = {this.state.userName} 
                              gameId = {this.state.gameId} 
                              mathProblems = {this.state.mathProblems} 
                              userId = {this.state.userId} 
                              time = {this.props.location.state.time} 
                              isMultiplayerGame={true}/>
                    </div>   
                }
            </div>
        );
    }
}

export default Multiplayer;