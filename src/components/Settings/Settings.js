import React, { Component } from 'react';
import {Link} from "react-router-dom";
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';


class Settings extends Component {

    constructor(props) {
        super(props);
        
    }

    render() {
        return (
            <div>
                <div className="flexbox-div">
                    <label htmlFor="selectTime">How much time would you like?</label>
                    <DropDownMenu value={this.props.time} onChange={this.props.setTime} id="selectTime">
                        <MenuItem value={1} primaryText="One minute" />
                        <MenuItem value={2} primaryText="Two minutes" />
                        <MenuItem value={3} primaryText="Three minutes" />
                    </DropDownMenu>
                </div>
       
                <div className="flexbox-div">
                    <label htmlFor="selectDifficulty">Choose a difficulty level</label>
                    <DropDownMenu value={this.props.difficulty} onChange={this.props.setDifficulty} id="selectDifficulty">
                        <MenuItem value={1} primaryText="Easy" />
                        <MenuItem value={2} primaryText="Medium" />
                        <MenuItem value={3} primaryText="Hard" />
                    </DropDownMenu>
                </div>
            </div>
        );
    }
}

export default Settings;