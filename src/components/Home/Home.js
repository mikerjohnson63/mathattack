import React from 'react';
import {Link} from "react-router-dom";
import 'typeface-roboto';
import RaisedButton from 'material-ui/RaisedButton';

const style = { margin: 12};
const Home = () => (
    <div>
        <h1>Welcome to Math Attack</h1>
        <p>The interactive math learning game</p>
        <RaisedButton label='Single Player' primary={true} containerElement={<Link to='/single'/>} style={style}/>
        <RaisedButton label='Multiplayer' primary={true} containerElement={<Link to='/multi'/>} style={style}/>
    </div>
    
    

);

export default Home;
