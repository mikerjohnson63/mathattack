import React, {Component} from 'react';
import CountdownTimer from './CountdownTimer';
import SinglePlayerResults from './SinglePlayerResults';
import MathProblemDisplay from './MathProblemDisplay';
import CountdownToGameStart from './CountdownToGameStart';
import MathProblemsApi from '../../utilities/MathProblemsApi';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';

const correctAnswerResponses=["Correct!","Good job!","That's right!","Nice one!","Way to go!"];
const incorrectAnswerResponses=["Wrong answer!","Sorry, that's wrong","Incorrect!","Nope, sorry!"];

class Play extends Component{
    constructor(props){
        super(props);

        this.state = 
        {
            inputValue:"", 
            correctAnswerCount: 0, 
            incorrectAnswerCount: 0,
            totalQuestionCount: 0,
            isTimeUp: false,
            currentProblemIndex: 0,
            readyToPlay: false,
            previousAnswerResult: '',
            message:'',
            playerStats : []
        };
    }

    componentDidMount(){
        if(this.props.isMultiplayerGame){
            //const  socket = new SockJS('https://attack-server.cfapps.io/gs-guides-websocket');
            const  socket = new SockJS('http://localhost:8080/gs-guides-websocket');

            this.stompClient = Stomp.over(socket);
            this.stompClient.connect({}, (frame) => {

                this.stompClient.subscribe('/mathAttack/multiplayer/' + this.props.gameId + '/gameInfo', gameInfo => {
                    
                    this.setState({
                        message: parsedGameInfo.message,
                        playerStats: JSON.parse(gameInfo.body).players,
                    });
                }); 
                
                let message = ' ';
                this.stompClient.send('/app/gameServer/multiplayer/' + this.props.gameId + '/getGameInfo', {}, message);
            });
        }
    }

    //Scores the current question and goes to the next one
    goToNextQuestion(isCorrectAnswer) {
        
        let correctAnswerCount = this.state.correctAnswerCount;
        let incorrectAnswerCount = this.state.incorrectAnswerCount;
        let totalQuestionCount = this.state.totalQuestionCount;

        //Type coercion allows you to compare a string to a number to see if answers match
        if(isCorrectAnswer){
            //update the correctAnswerCount
            correctAnswerCount++;
            this.setPreviousAnswerCorrect(true);
        }
        else{
            //update the incorrectAnswerCount
            incorrectAnswerCount++;
            this.setPreviousAnswerCorrect(false);
        }

        //update the total question count regardless of the user's accuracy
        totalQuestionCount++;

        //set the state with the newly updated values
        this.setState({
            correctAnswerCount: correctAnswerCount,
            incorrectAnswerCount: incorrectAnswerCount,
            totalQuestionCount: totalQuestionCount,

            //If the user somehow went through all the questions, start the index back at the beginning.  Otherwise, increment it by 1.
            currentProblemIndex: ((this.state.currentProblemIndex + 1) % this.props.mathProblems.length) === 0 ? 
                0 : this.state.currentProblemIndex + 1
        });
    }

    goToNextMultiplayerQuestion(isCorrectAnswer) {

        //Update the database
        this.mathProblemsApi = new MathProblemsApi();
        this.mathProblemsApi.answerSubmitted(this.props.userId,isCorrectAnswer, (response) => {
            this.setState({
                correctAnswerCount: response.correctAnswerCount,
                incorrectAnswerCount: response.incorrectAnswerCount,
                totalQuestionCount: response.correctAnswerCount + response.incorrectAnswerCount
            })

        let message = response.userName + " answered a question " + (isCorrectAnswer ? "correctly!" : "incorrectly.");
        this.stompClient.send('/app/gameServer/multiplayer/' + this.props.gameId + '/getGameInfo', {}, message);
        
        });

        if(isCorrectAnswer){
            this.setPreviousAnswerCorrect(true);
        }
        else{
            this.setPreviousAnswerCorrect(false);
        }

        // //set the state with the newly updated values
        this.setState({
            //If the user somehow went through all the questions, start the index back at the beginning.  Otherwise, increment it by 1.
            currentProblemIndex: ((this.state.currentProblemIndex + 1) % this.props.mathProblems.length) === 0 ? 
                0 : this.state.currentProblemIndex + 1
        });
    }

    //updates the inputValue state every time the user makes a change to the input
    updateInputValue(evt) {
        this.setState({inputValue: evt.target.value});
    }

    //When the user hits "Enter", submit the answer
    handleKeyPress = (event) => {
        if(event.key === "Enter"){
            this.handleQuestionSubmit();
        }
    }

    //set the state of the isTimeUp boolean to true
    handleTimeUp(){
        this.setState({isTimeUp: true});
    }

    //reset the input value and make a new question
    handleQuestionSubmit(){

        let isCorrectAnswer = Number(this.state.inputValue) === this.props.mathProblems[this.state.currentProblemIndex].solution;
        this.setState({inputValue: ""});

        if(this.props.isMultiplayerGame){
            this.goToNextMultiplayerQuestion(isCorrectAnswer);
        }
        else{
            this.goToNextQuestion(isCorrectAnswer);
        }
    }

    setPreviousAnswerCorrect(result){

        let response;

        if(result){
            response = correctAnswerResponses[Math.floor(Math.random() * correctAnswerResponses.length)];
        }
        else{
            response = incorrectAnswerResponses[Math.floor(Math.random() * incorrectAnswerResponses.length)];
        }

        this.setState({
            previousAnswerResult: response
        })
    }

    requestNextGame(winnerId){
        this.stompClient.send('/app/gameServer/multiplayer/' + this.props.gameId + '/requestNextGame', {}, winnerId);
    }
    
    render(){
        return(
                <div>
                    {this.props.mathProblems && this.props.mathProblems.length > 0 &&                      
                        <div>
                            {!this.state.isTimeUp ?
                            ( 
                                <div>
                                    <h1>Math problems</h1>
                    
                                    {this.props.isMultiplayerGame &&
                                        <div>
                                            <p>Your user name is: {this.props.userName}</p>
                                            <div>{this.state.message}</div>
                                            <br/>
                                        </div>
                                    }

                                    <h3>Time Remaining:&nbsp;
                                    <CountdownTimer onTimeUp={this.handleTimeUp.bind(this)}
                                                    timeRequested={this.props.time * 60}
                                                    showSecondsOnly={false}
                                                    isMultiplayerGame={this.props.isMultiplayerGame}
                                                    gameId={this.props.gameId}/>
                                    </h3>
                                    <MathProblemDisplay question={this.props.mathProblems[this.state.currentProblemIndex]}
                                                        correctAnswerCount={this.state.correctAnswerCount}
                                                        incorrectAnswerCount={this.state.incorrectAnswerCount}
                                                        inputValue={this.state.inputValue}
                                                        previousAnswerResult={this.state.previousAnswerResult}
                                                        onUpdateInputValue={this.updateInputValue.bind(this)}
                                                        onKeyPress={this.handleKeyPress.bind(this)}
                                                        onQuestionSubmit={this.handleQuestionSubmit.bind(this)}/>

                                    <div>
                                        {this.props.isMultiplayerGame &&
                                            <div>
                                                <h3>Players</h3>

                                                <div className="flexbox-div">
                                                    <span className="flex-element"><strong>User Name</strong></span>
                                                    <span className="flex-element"><strong>Correct Answers</strong></span>
                                                    <span className="flex-element"><strong>Incorrect Answers</strong></span>
                                                </div>
                                                <br/>
                                            </div>
                                        }

                                        {this.state.playerStats.map((player,index) => (
                                            <div key={index}>
                                                <div className="flexbox-div">
                                                    <span className="flex-element">{player.userName}</span>
                                                    <span className="flex-element">{player.correctAnswerCount}</span>
                                                    <span className="flex-element">{player.incorrectAnswerCount}</span>
                                                </div>
                                                <br/>
                                            </div>
                                        ))}         
                                    </div>
                                </div>
                            )
                            :
                            (
                                <div>
                                    <SinglePlayerResults percentage={Math.floor((this.state.correctAnswerCount / 
                                                            this.state.totalQuestionCount) * 100)}
                                                         correctAnswerCount={this.state.correctAnswerCount}
                                                         incorrectAnswerCount={this.state.incorrectAnswerCount}
                                                         playerStats={this.state.playerStats}
                                                         isMultiplayerGame={this.props.isMultiplayerGame}
                                                         gameId={this.props.gameId}
                                                         onNextGame={this.requestNextGame.bind(this)}/>
                                </div> 
                            )
                        }
                        </div>       
                    }
                </div>
        )
    }
}

export default Play;
