import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import MathProblemsApi from '../../utilities/MathProblemsApi';
import RaisedButton from 'material-ui/RaisedButton';

const style = { margin: 12};

const lowScoreResponses=[
    "You scored on the same level as a bag of rocks!",
    "Were you actually trying this time?",
    "Time to dust off the math books!"];

const mediumToLowScoreResponses=[
    "Hmmm... Well there's definitely room for improvement!",
    "Practice makes perfect. Let's get to it!",
    "You've still got a ways to go.  Let's try again!",
];

const mediumToHighScoreResponses=[
    "Not bad!  A bit more practice and you'll be a pro!",
    "Good!  You're getting there!",
    "Almost there.  Let's practice just a bit more!"
];

const highScoreResponses=[
    "Wow, nice job!",
    "Einstein's got nothing on you!",
    "You must have done this kind of thing before!",
    "Your math teacher would be proud!",
    "You're officially a basic math pro!"
];

class SinglePlayerResults extends Component{
    constructor(props){
        super(props);

        this.state={
            scoreResponse: "",
            gameWinner: "",
            resetState: false,
            winnerId: -1
        };
    }

    componentDidMount(){
        this.setScoreResponse();

        if(this.props.isMultiplayerGame){

            if(this.props.playerStats.length > 0){
                let highScoringPlayer = this.props.playerStats[0];
                let isTie = false;

                this.props.playerStats.forEach(function(player){
                    if(player !== highScoringPlayer){
                        let adjustedScore = player.correctAnswerCount - player.incorrectAnswerCount;
                        if(adjustedScore > (highScoringPlayer.correctAnswerCount - highScoringPlayer.incorrectAnswerCount)){
                            highScoringPlayer = player;
                            isTie = false;
                        }
                        else if(adjustedScore === (highScoringPlayer.correctAnswerCount - highScoringPlayer.incorrectAnswerCount)){
                            isTie = true;
                        }
                    }
                });

                if(isTie){
                    this.setState({
                        gameWinner: "Looks like we have a tie this round! No wins are awarded this time."
                    })
                }
                else{
                    let score = highScoringPlayer.correctAnswerCount - highScoringPlayer.incorrectAnswerCount;
                    this.setState({
                        gameWinner: "Our winner is " + highScoringPlayer.userName + " with an adjusted score of " + score + " points!",
                        winnerId: highScoringPlayer.id
                    }) 
                }

                let winnerId = this.state.winnerId;
                this.props.playerStats.forEach(function(player){
                    if(player.id === winnerId){
                        player.numberOfWins++;
                    }
                });

                //this.incrementNumberOfWins(highScoringPlayer.id);
            }
        }
    }

    setScoreResponse(){
        var randomNumber = Math.random();
        let percentage = isNaN(this.props.percentage) ? 0 : this.props.percentage;
        let scoreResponse;

        if(percentage < 20){
            scoreResponse = lowScoreResponses[Math.floor(randomNumber * lowScoreResponses.length)];
        }
        else if(percentage < 60){
            scoreResponse = mediumToLowScoreResponses[Math.floor(randomNumber * mediumToLowScoreResponses.length)];
        }
        else if(percentage < 90){
            scoreResponse = mediumToHighScoreResponses[Math.floor(randomNumber * mediumToHighScoreResponses.length)];
        }
        else{
            scoreResponse = highScoreResponses[Math.floor(randomNumber * highScoreResponses.length)];
        }

        this.setState({
            scoreReponse: scoreResponse
        });
    }

    render(){
        return(
            <div>
                <h1>Time's Up!</h1>
                
                {this.props.isMultiplayerGame ? 
                    <div>
                        <h2>{this.state.gameWinner}</h2>
                        <h3>Game Results</h3>
                        
                        <div className="flexbox-div">
                            <span className="flex-element"><strong>User Name</strong></span>
                            <span className="flex-element"><strong>Total Score</strong></span>
                            <span className="flex-element"><strong>Number of Wins</strong></span>
                        </div>
                        <div>{this.props.playerStats.map((player,index) => (
                            <div className="flexbox-div" key={index}>
                                <span className="flex-element">{player.userName}</span>
                                <span className="flex-element">{player.correctAnswerCount - player.incorrectAnswerCount}</span> 
                                <span className="flex-element">{player.numberOfWins}</span>
                            </div>
                            ))}         
                        </div>
                        <br/>
                    </div>
                    :
                    <div>      
                        <h3>{this.state.scoreReponse}</h3>
                        <div className="flexbox-div">
                            <span className="correct-and-incorrect-display">
                                Correct answers: {this.props.correctAnswerCount}
                            </span>
                            <span className="correct-and-incorrect-display">
                                Incorrect answers: {this.props.incorrectAnswerCount}
                            </span>
                            <span className="correct-and-incorrect-display">
                                Accuracy: {isNaN(this.props.percentage) ? 0 : this.props.percentage}%
                            </span>
                        </div>
                    </div>
                }  

                <div>
                    {this.props.isMultiplayerGame ?
                        <RaisedButton label='Play again' onClick={() => this.props.onNextGame(this.state.winnerId)} primary={true} style={style}/>
                    :
                        <RaisedButton label='Play again' primary={true} containerElement={<Link to='/single'/>} style={style}/>
                    }
                    &nbsp;

                    <RaisedButton label='Go to Main Page' primary={true} containerElement={<Link to='/'/>} style={style}/>
                </div>
            </div>
        );
    }
}

export default SinglePlayerResults;