import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class MathProblemDisplay extends Component{
   
    render(){
        return(
            <div>
                <h3>
                    <span>
                        {this.props.question.firstNum}&nbsp;
                        {this.props.question.operator}&nbsp;
                        {this.props.question.secondNum}
                    </span>
                </h3>
                
                <div>
                    <TextField 
                        hintText="Answer"
                        type="number"
                        id="user-response"
                        value={this.props.inputValue} 
                        autoFocus
                        onKeyPress={evt => this.props.onKeyPress(evt)}
                        onChange={evt => this.props.onUpdateInputValue(evt)}/>

                        &nbsp;
                        <RaisedButton onClick={() => this.props.onQuestionSubmit()} label='Submit' />
                </div>
                <div>
                    {this.props.previousAnswerResult}
                </div>
                <div className="flexbox-div">
                    <span className="correct-and-incorrect-display">Correct Answers:&nbsp;{this.props.correctAnswerCount}</span>
                    <span className="correct-and-incorrect-display">Incorrect Answers:&nbsp;{this.props.incorrectAnswerCount}</span>
                </div>
            </div>
        );
    }
}

export default MathProblemDisplay;