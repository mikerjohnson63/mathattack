import React, {Component} from 'react';
import CountDownTimer from '../Play/CountdownTimer';
import RaisedButton from 'material-ui/RaisedButton';


class CountdownToGameStart extends Component{

    constructor(props){
        super(props);

        this.state={
            readyToPlay: false
        }
    }

    componentDidMount(){  
        if(!this.props.waitUntilClick){
            this.handleReadyToPlay();
        }
    }

    handleReadyToPlay(){
        this.setState({
            readyToPlay: true
        });
    }

    handleTimeUp(){
            this.props.setGameStarted();
    }

    render(){
        return(
            <div>
                <h2>Game Rules</h2>
                <p>No calculators!</p>
                <p>Feel free to use scratch paper to solve tricky problems.</p>
                <p>Your score is determined by correct <i>and</i> incorrect answers.</p>           
                <br/>

                {!this.state.readyToPlay ? 
                    <RaisedButton label='Ready to Play' onClick={this.handleReadyToPlay.bind(this)} primary={true} />
                    :
                    <div>
                        <h3>
                            Game will start in <CountDownTimer onTimeUp={this.handleTimeUp.bind(this)} 
                            timeRequested={this.props.secondsRequested}
                            showSecondsOnly={true}
                            isMultiplayerGame={this.props.isMultiplayerGame}/>
                        </h3>
                    </div>
                }
            </div>
        );
    }
}

export default CountdownToGameStart;