import React, {Component} from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

class CountdownTimer extends Component{
    constructor(props){
        super(props);

        this.state = {
            timeRemaining: this.props.timeRequested,
            secondsRemaining: 0,
            minutesRemaining: 0,
            startedTimer: Date.now(),
        };
    }

    //Set up an interval that will call 'tick' every second
    componentDidMount(){

        this.setMinutesAndSeconds(this.state.timeRemaining);
        this.interval = setInterval(() => this.tick(), 1000);
    }

    //Stop the interval
    componentWillUnmount(){
        clearInterval(this.interval);
    }

    //Updates the timer every second
    tick(){
        let intervalCleared = false;
        let timeRemaining = this.state.timeRemaining;

        if(timeRemaining > 0){

            let tempTimer = Math.floor((Date.now() - this.state.startedTimer) / 1000);
            timeRemaining = this.props.timeRequested - tempTimer;

                this.setMinutesAndSeconds(timeRemaining);           
        }
        else{
            clearInterval(this.interval);
            this.props.onTimeUp();
        }
    }

    setMinutesAndSeconds(timeRemaining){
        this.setState({
            secondsRemaining: timeRemaining % 60,
            minutesRemaining: timeRemaining > 0 ? Math.floor(timeRemaining / 60) : 0,
            timeRemaining: timeRemaining
        })
    }

    render(){
        return(
            <span>
                {this.props.showSecondsOnly ?
                    <span>{this.state.secondsRemaining}</span>
                :
                    <span>
                        {this.state.minutesRemaining}:{this.state.secondsRemaining >= 10 ? 
                        this.state.secondsRemaining : '0' + this.state.secondsRemaining}
                    </span>
                }
            </span>
        );
    }
}

export default CountdownTimer;