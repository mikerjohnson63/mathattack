import axios from 'axios';

class MathProblemsApi {

    constructor() {
         this.baseUrl = "http://localhost:8080";
        //this.baseUrl = "https://attack-server.cfapps.io";
    }

    getMathProblems(difficulty, time, callback) {
        axios.get(this.baseUrl + "/mathProblems",{params:{difficulty: difficulty,
        time: time}
        })
        .then((response) => {
            callback(response.data);
        }).catch( (error) => {
            console.log("Error creating new game!: " + error);
        });
    }

    setUserIsReadyToPlay(userId,callback){
        axios.get(this.baseUrl + "/mathProblems/readyToPlay",{params:{userId:userId}
        })
        .then((response) => {
            callback(response.data);
        }).catch((error) => {
            console.log("ERROR!: " + error);
        });
    }

    answerSubmitted(userId, isCorrectAnswer, callback){
        axios.get(this.baseUrl + "/mathProblems/answerSubmitted", {params:{userId:userId, isCorrectAnswer:isCorrectAnswer}
        })
        .then((response) => {
            callback(response.data);
        }).catch((error) => {
            console.log("ERROR!: " + error);
        });
    }


    createNewGame(difficulty,time,callback){
        axios.get(this.baseUrl + "/mathProblems/newGame",{params:{difficulty: difficulty,
        time: time}
    })
    .then((response) => {
        callback(response.data);
    }).catch((error) => {
        console.log("ERROR!!: " + error);
    });
    }
}

export default MathProblemsApi;