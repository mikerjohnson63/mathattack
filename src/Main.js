import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './components/Home/Home';
import SettingsRoutes from './SettingsRoutes';
import Settings from './components/Settings/Settings';
import MultiplayerMenu from './components/Multiplayer/MultiplayerMenu';
import Multiplayer from './components/Multiplayer/Multiplayer';

const Main = () => (
    <div>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/single' component={SettingsRoutes}/>
            <Route exact path='/multi' component={MultiplayerMenu}/>
            <Route path='/multi/settings' component={Settings}/>
            <Route path='/multi/play' component={Multiplayer}/>
            {/* <Route path='/rules' component={SettingsRoutes}/> */}
            {/* <Route path='/about' component={SettingsRoutes}/> */}
            {/* <Route path='/contact' component={SettingsRoutes}/> */}
        </Switch>
    </div>
)


export default Main;