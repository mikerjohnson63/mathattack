import React from 'react';
import SinglePlayer from "./components/SinglePlayer/SinglePlayer";
import SinglePlayerMenu from "./components/SinglePlayer/SinglePlayerMenu";
import {Route, Switch} from "react-router-dom";

const SettingsRoutes = () => (
    <div>
        <Switch>
        <Route exact path='/single' component={SinglePlayerMenu}/>
        <Route exact path='/single/play' component={SinglePlayer}/>
        </Switch>
    </div>
);

export default SettingsRoutes;